var result;
var subheader;
function getTableData(reportName, tableName,agreement_number,callback, elem, widElem, configureDataTable,Year ,agreement_name) {
	
	$.ajax({
        url: '/getTableData/',
        data : {
            report : reportName,
            table : tableName,
            agreementNumber : agreement_number,
            'Year' : Year,
            'agreement_name' : agreement_name,
        } ,
        dataType : 'json',
        beforeSend : function(){
           
        },
        success: function(result){
        	//console.log('==================')
        	//console.log(result);
        	//console.log('---------------------------------')
            callback(result, elem, widElem, configureDataTable);
           
            //console.log(callback)

        },
        error: function(jqXHR, textStatus, errorThrown) {},
        complete: function(request, textStatus) { 
        	
        	

        }
    });
    
}

function getTrimmedName(name, pattern){
    
    word_list = name.split(" ");
    new_word = "";
    
    for(i in word_list){
        
        new_word = new_word + word_list[i].substr(0, 3) + " ";
        
    }
    
    return new_word;

}

function createHeader(headers){

    ////console.log(headers);

    tr = $('<tr></tr>');
    
    for(head in headers){
        th = $('<th></th>');
        th.html(headers[head]);
        tr.append(th);
    }

    return tr;
}
function createFooter(footer)
{
    tr = $('<tr></tr>');
    
    for(head in footer){
        th = $('<th></th>');
        th.html(footer[head]);
        tr.append(th);
    }

    return tr;
}

function createTableBody(result,elem){
	
	//console.log(elem);
	
	rows = result['data'];
	headers = result._headers;
	tbody = $('#'+ elem).find('tbody');
	
	for(row in rows){
		tr = $('<tr></tr>');
		
		for(i=0; i< headers.length; i++){
			
			td = $('<td></td>');
			if(headers[i][0] == "_"){
				
				td.html(JSON.stringify(rows[row][headers[i]]));
			}
			else{
				td.html(rows[row][headers[i]]);
			}
			tr.append(td);
		}
		tbody.append(tr);
	}
}
function createHeaderreport4RPY(header,header_format){

   
    var tdata = new Array();
    var tr1 = $('<tr></tr>');
    var tr = $('<tr></tr>');
    var data_var  = 1
    var prev_len = 0
    var minus = 0
    var counter = 0;
        for(key in header_format)
        {
            
            //console.log(Object.keys(header))
            //console.log(header)
            //console.log(header_format)
            //console.log(header[header_format[key]])
            //console.log(key)
            //console.log("===========<><><><>=====")
            
            if(header[header_format[key]][1] > 1)
                        {
                            
                            o = parseInt(prev_len) + parseInt(key)  - parseInt(minus)
                            //console.log( "prev_len::"+ prev_len)
                            prev_len += header[header_format[key]][1]
                            data_var =  parseInt(header[header_format[key]][1]) + parseInt(key)
                            //console.log("index:"  + key)
                            //console.log("length::" + header[header_format[key]][1])
                            //console.log("sum" + o)
                            //console.log("-------------------------------------------") 
                           $(tr).find('th:last').append('<span class="expand-col-prevyear " data-subheader="'+header[header_format[key]][1]+'" data-index="'+o+'" ><img src="/static/img/details_open.png" /></span>');
                          $(tr).find('th:last').find('.expand-col-prevyear').bind('click' ,expandyear);
                            minus++;
                            counter = counter + parseInt(header[header_format[key]][1]) - 1;
                        }

            th = $('<th data-id="'+counter+'" rowspan="'+header[header_format[key]][0]+'" colspan="'+header[header_format[key]][1]+'"></th>');
          try{
                    var s = header_format[key];
                    var n = s.indexOf('_');
                    data_html = s.substr(0, n != -1 ? n : s.length);
          }
          catch(err)
          {

          }
            
            th.html(data_html);
          
           //console.log("<>>> <<<>>>>>> <>>>>>>>" + header_format[key])
           //console.log(s);
           //console.log(n);
           //console.log(data_html);
           //console.log("<>>> <<<>>>>>> <>>>>>>>")
    
           
            tr.append(th);
                 if(header[header_format[key]][1] > 1)
                        {
                           $(tr).find('th:last').addClass('back-frame')
                          
                        }

                        counter = counter + 1;
        }
     
    


    for(key in header_format)
        {
            
            if(header[header_format[key]][2] != null)
            {
                //console.log(header[header_format[key]][2])
                k = 0;
                for(subdata in header[header_format[key]][2])
                {
                    //console.log(header[header_format[key]][2][subdata])
                    th = $('<th rowspan="'+header[header_format[key]][2][subdata][0]+'" colspan="'+header[header_format[key]][2][subdata][1]+'"></th>');
                    try{
                                    var s = Object.keys(header[header_format[key]][2])[k];
                                    var n = s.indexOf('_');
                                    data_html = s.substr(0, n != -1 ? n : s.length);
                      }
                      catch(err)
                      {

                      }
                    th.html(data_html);
                    //console.log(Object.keys(header[header_format[key]][2])[k]);
                    //console.log("<<<<<<<<<>>>>>>>>>");
                        tr1.append(th);
                        k = k+1;
                }
            

            }
            

        }
    tr1.css( {'background': 'lightgray'});

     $(tr).find('th').css('vertical-align',' middle');
    tdata[0] = tr
    tdata[1] = tr1
    return tdata;
}
function createHeaderreport4(header,header_format){

   
    var tdata = new Array();
    var tr1 = $('<tr></tr>');
    var tr = $('<tr></tr>');
   
    	for(key in header_format)
    	{
    	           //console.log(header[header_format[key]])
                        if(header[header_format[key]][1] > 1)
                        {
                           $(tr).find('th:last').append('<span class="expand-col " data-subheader="'+header[header_format[key]][1]+'" data-index="'+key+'" ><img src="/static/img/details_open.png" /></span>')
                            $(tr).find('.expand-col').click(expand);
                            if($(tr).find('th:last').html().substr(0,14) == "Agreement Size"){
                                 $(tr).find('th:last').append('<br/>USD');
                            }
                        }
    		th = $('<th rowspan="'+header[header_format[key]][0]+'" colspan="'+header[header_format[key]][1]+'"></th>');
       		th.html(header_format[key]);
                            th.addClass('r4-summary-header')
        		tr.append(th);
                          if(header[header_format[key]][1] > 1)
                        {
                           $(tr).find('th:last').addClass('back-frame')
                        }

    	}
               
	for(key in header_format)
    	{
    		
    		if(header[header_format[key]][2] != null)
    		{
    			//console.log(header[header_format[key]][2])
          
    			k = 0;
    			for(subdata in header[header_format[key]][2])
    			{
                                                 
    				th = $('<th rowspan="'+header[header_format[key]][2][subdata][0]+'" colspan="'+header[header_format[key]][2][subdata][1]+'"></th>');
       				
                                                      try{
                                                            var s = Object.keys(header[header_format[key]][2])[k];
                                                            var n = s.indexOf('_');
                                                            data_html = s.substr(0, n != -1 ? n : s.length);
                                                      }
                                                      catch(err)
                                                      {

                                                      }
                                            
                                                     th.html(data_html);
                                                    //th.html(Object.keys(header[header_format[key]][2])[k]);
       				//console.log(Object.keys(header[header_format[key]][2])[k]);
       				//console.log("<<<<<<<<<>>>>>>>>>");
        				tr1.append(th);
        				k = k+1;
    			}
    		

    		}
    		

    	}

    tr1.css( {'background': 'lightgray'});
    
     $(tr).find('th').css('vertical-align',' middle');
    tdata[0] = tr;
    tdata[1] = tr1;
    console.log($(tdata[0]));
    return tdata;
}
function createTableBodyreport4(result,elem)
{
	////console.log(elem);
	
	rows = result['data'];
	//headers = result._headers;
	//subheader = result._subheader;
	tbody = $('#'+ elem).find('tbody');

	for(row in rows){
		tr = $('<tr></tr>');

		
		for (data_item in  rows[row])
		{
			
			td = $('<td></td>');
			td.html(rows[row][data_item]);
			tr.append(td);
		}
	
		
		tbody.append(tr);
	}


}
function createHeaderreport5CAT(header,header_format){

   
    var tdata = new Array();
    var tr1 = $('<tr></tr>');
    var tr = $('<tr></tr>');
    var data_var  = 1
    var prev_len = 0
    var minus = 0
        for(key in header_format)
        {
            
            if(header[header_format[key]][1] > 1)
                        {
                            
                            o = parseInt(prev_len) + parseInt(key)  - parseInt(minus)
                            //console.log( "prev_len::"+ prev_len)
                            prev_len += header[header_format[key]][1]
                            data_var =  parseInt(header[header_format[key]][1]) + parseInt(key)
                      
                           $(tr).find('th:last').append('<span class="expand-col-prevyear" data-subheader="'+header[header_format[key]][1]+'" data-index="'+o+'" ><img src="/static/img/details_open.png" /></span>');
                       
                            minus++;
                        }

                     

                    th = $('<th rowspan="'+header[header_format[key]][0]+'" colspan="'+header[header_format[key]][1]+'"></th>');
                  try{
                            var s = header_format[key];
                            var n = s.indexOf('_');
                            data_html = s.substr(0, n != -1 ? n : s.length);
                      }
                      catch(err)
                      {

                      }
            
                     th.html(data_html);
                    tr.append(th);
                    if(header[header_format[key]][3] != null)
                    {
                         if(header[header_format[key]][4] != null)
                         {
                            $(tr).find('th:last').append('<span class="expand-col-list" data-parent="'+header[header_format[key]][4]+'" data-list="'+header[header_format[key]][3]+'" ><img src="/static/img/details_open.png" /></span>');     
                         }
                         else
                         {
                            $(tr).find('th:last').append('<span class="expand-col-list" data-list="'+header[header_format[key]][3]+'" ><img src="/static/img/details_open.png" /></span>');     
                         }
                       
                    }
                 
                 if(header[header_format[key]][1] > 1)
                        {
                           $(tr).find('th:last').addClass('back-frame');
                        }


        }
     
    


    for(key in header_format)
        {
            
            if(header[header_format[key]][2] != null)
            {
                //console.log(header[header_format[key]][2])
                k = 0;
                for(subdata in header[header_format[key]][2])
                {
                    //console.log(header[header_format[key]][2][subdata])
                    th = $('<th rowspan="'+header[header_format[key]][2][subdata][0]+'" colspan="'+header[header_format[key]][2][subdata][1]+'"></th>');
                    th.html(Object.keys(header[header_format[key]][2])[k]);
                    //console.log(Object.keys(header[header_format[key]][2])[k]);
                    //console.log("<<<<<<<<<>>>>>>>>>");
                        tr1.append(th);
                        k = k+1;
                }
            

            }
            

        }
    tr1.css( {'background': 'lightgray'});

     $(tr).find('th').css('vertical-align',' middle');
    tdata[0] = tr
    tdata[1] = tr1
    return tdata;
}
function formatDataReport5CAT(result, elem, widElem, configureDataTable){
          //console.log("elem name " + elem ) 
    
    $('#'+ widElem).find('header h2').text(result['_meta']['_table_title']);

    tr1 =  createFooter(result['_footers'])
    $('#'+ elem).find('tfoot').append(tr1);
    // make headers 
  
    tr = createHeaderreport5CAT(result['_header'],result['_header_format']);
    $('#'+ elem).find('thead').append(tr);
    
    // make table body
    createTableBodyreport4(result, elem);
    
    //configure DataTable
    configureDataTable(elem, result);

}

 


function formatDataReport4(result, elem, widElem, configureDataTable){

	
    $('#'+ widElem).find('header h2').text(result['_meta']['_table_title']);
	
    // make headers 
  
    tr = createHeaderreport4(result['_header'],result['_header_format']);

    $('#'+ elem).find('thead').append(tr[0]);
    
    $('#'+ elem).find('thead').append(tr[1]);
    // make table body
    createTableBodyreport4(result, elem);
    
    // making table footer
    tr1 =  createFooter(result['_footers'])
    $('#'+ elem).find('tfoot').append(tr1);
    //configure DataTable
   
    configureDataTable(elem, result);

}


function formatDataReport4RPY(result, elem, widElem, configureDataTable){
 //console.log("elem name " + elem ) 
    
    $('#'+ widElem).find('header h2').text(result['_meta']['_table_title']);

    
    // make headers 
  
    tr = createHeaderreport4RPY(result['_header'],result['_header_format']);
      $('#'+ elem).find('thead').append(tr[0]);
    $('#'+ elem).find('thead').append(tr[1]);
    
    // make table body
    createTableBodyreport4(result, elem);

    tr1 =  createFooter(result['_footers'])
    $('#'+ elem).find('tfoot').append(tr1);
    //configure DataTable
    configureDataTable(elem, result);



}
function formatDataReport5(result, elem, widElem, configureDataTable){
          //console.log("elem name " + elem ) 
    
    $('#'+ widElem).find('header h2').text(result['_meta']['_table_title']);
    
    // make headers 
      tr = createHeader(result['_headers']);
    $('#'+ elem).find('thead').append(tr[0]);
    $('#'+ elem).find('thead').append(tr[1]);
    
    tr1 =  createFooter(result['_footers'])
    $('#'+ elem).find('tfoot').append(tr1);
    // make table body
    createTableBodyreport4(result, elem);
    
    //configure DataTable
    configureDataTable(elem, result);

}
function formatData(result, elem, widElem, configureDataTable){
	//console.log("elem name " + elem 	) 
	
    $('#'+ widElem).find('header h2').text(result['_meta']['_table_title']);

    // make headers 
    tr = createHeader(result['_headers']);
    $('#'+ elem).find('thead').append(tr);
    
    // make table body
    createTableBody(result, elem);
    
    configureDataTable(elem, result);
}

function formatDataR4(result, elem, widElem, configureDataTable){

    
    $('#'+ widElem).find('header h2').text(result['_meta']['_table_title']);

    // make headers 
    tr = createHeader(result['_headers']);
    $('#'+ elem).find('thead').append(tr);
    
    // make table body
    createTableBodyreport4(result, elem);
    
    tr1 =  createFooter(result['_footers'])
    $('#'+ elem).find('tfoot').append(tr1);

    configureDataTable(elem, result);
}
function formatTitle(ajaxResponse, var2, title, date){
	
	$('#' + title +' li').eq(0).html(ajaxResponse['report']);
	$('#' + title +' li').eq(1).html(ajaxResponse['name']);
	$('#' + date).html(formatDate(ajaxResponse['date']));
	
}

function formatSubReportTitle(ajaxResponse, var2, title, date){
	//console.log(ajaxResponse['name']);
	//console.log(var2);
	//console.log(title);
	$('#' + title +' li').eq(0).html(ajaxResponse['report']);
	$('#' + title +' li').eq(1).html(agreement_number);
	$('#' + title +' li').eq(2).html(ajaxResponse['name']);
	$('#' + date).html(formatDate(ajaxResponse['date']));
}


function handleClickOfDetailsControl(dataTableName,handleCallback){
	dataTableName.on('click', 'td.details-control', function () {


	    var tr = $(this).closest('tr');
	    
	    var api = dataTableName.api();  
	    
        var row = api.row( tr );
        
       if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( handleCallback(row.data())).show();
            tr.addClass('shown');
        }
    });
}

function formatDate(unixCode){
	
	formattedDate = moment.unix(parseInt(unixCode)).format('DD-MMM-YY');
	if(formattedDate == 'Invalid date'){
		formattedDate = "";
	}
	
	return formattedDate
}

function giveTitleToWidgetCustom(widElem, data){
	$('#'+ widElem).find('header h2').text(data);
}

function allValuesSame(array){

    if(array.length > 0) {
        for(var i = 1; i < array.length; i++)
        {
            if(array[i] !== array[0])
                return false;
        }
    } 
    return true;
}
