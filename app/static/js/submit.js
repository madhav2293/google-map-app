$(document).ready(function(){
$("#wid-id-4 .appheader h2").text('Applications');
$(".formheader h2").text('Submit Application');
$(".select-passyear").select2();
$(".select-gap").select2();
headerText = '<h4 class="header-text">Analysis Of Students Applying For Graduation</h4>'
$("#ribbon").append(headerText);

pageSetUp();
populatecities();
populateuni();

$(document).on('click', '.submit-app', submitApp);
$(document).on('click', '.searchApp', searchApp);
$(document).on('keyup', '.sMarks', maintainPercentage);

});

$(window).load(function(){
	$(".formheader h2").text('Submit Application');
});
function maintainPercentage(e){

			marks_length = $(".sMarks").val().length;
			
			if(marks_length == "5"){
				return false;
			}
			if (e.keyCode == 8) { 
				return false;
		    }
			if(marks_length == "3"){
					return false;
			}
			if(marks_length == "2"){
				console.log("true");
				marks_val_temp = $(".sMarks").val() + "." ;
				$(".sMarks").val(marks_val_temp);
			}
			
}

function getGender(gender){
	genderDic = {'0': false, '1': true}
	return genderDic[gender] ;
}

function getGenderstr(genderstr){
	genderstrDic = {true: 'Female', false: 'Male'}
	return genderstrDic[genderstr] ;
}

function doAjax(url, type, data, contentType, dataType,  successCallback, errorCallback)
{
	$.ajax({
		url : url,
		type : type,
		data : data,
		contentType : contentType,
		dataType : dataType,
		success : function(data){
			if(successCallback){
				successCallback(data);
			}
		},
		beforeSend: function(jqXHR, settings) {
            // Pull the token out of the DOM.
            //jqXHR.setRequestHeader('X-CSRFToken', csrftoken);
        },
		error : function(data){
			if(errorCallback){
				errorCallback(data);
			}
			
		} 

	});	
}


function populatecities(){

		Cityurl = '/api/v1/city/'
		requestType = "GET";
		contentType = 'application/json';
		dataType= 'json';
		
		$(".select-city-1").select2();
		$(".select-city-2").select2();

		doAjax(Cityurl, requestType, '', contentType, dataType, appendCities);
		
		

}

function populateuni(){

		Uniurl = '/api/v1/uni/'
		requestType = "GET";
		contentType = 'application/json';
		dataType= 'json';
		
		doAjax(Uniurl, requestType, '', contentType, dataType, appendUni);
		
		$(".select-uni").select2();
		
}

function appendCities(data){
	console.log(data);
	citiesData =  data;
	citiesData_length = citiesData.objects.length;
	cityArraay = [] ;
	cityUriArraay = [] ;
	
	for(i=0;i<citiesData_length;i++){
		city = citiesData.objects[i].name ; 
		uri = citiesData.objects[i].resource_uri ;
		var idx = $.inArray(city, cityArraay);
		if (idx == -1) {
			cityArraay.push(city);
			cityUriArraay.push(uri);
			$('.select-city-1').append('<option value="'+ uri +'" data-uri="'+ uri +'">'+ city +'</option>');
			$('.select-city-2').append('<option value="'+ uri +'" data-uri="'+ uri +'">'+ city +'</option>')

		}
	}

	
}

function appendUni(data){
	console.log(data);
	uniData =  data;
	uniData_length = uniData.objects.length;
	uniArraay = [] ;
	uniUriArraay = [] ;
	
	for(i=0;i<uniData_length;i++){
		uni = uniData.objects[i].name ; 
		uniuri = uniData.objects[i].resource_uri ;
		var uidx = $.inArray(uni, uniArraay);
		if (uidx == -1) {
			uniArraay.push(uni);
			uniUriArraay.push(uniuri);
			$('.select-uni').append('<option selected="selected" value="'+ uniuri +'" data-uri="'+ uniuri +'">'+ uni +'</option>')

		}
	}
}

function submitApp(){
	submitPData = {};
	submitEData = {};

	ChoosenText = $(".select2-chosen").eq(0).text().length; 
	ChoosenText1 = $(".select2-chosen").eq(2).text().length;
	ChoosenText2 = $(".select2-chosen").eq(1).text().length; 


	if(ChoosenText == "1" || ChoosenText1 == "1" ){
		$('.form-success').addClass('hidden');
		$('.form-error').removeClass('hidden');
		$('.form-error').text(" * Please Choose City");
		return false ; 
	}

	if(ChoosenText2 == "1"){
		$('.form-success').addClass('hidden');
		$('.form-error').removeClass('hidden');
		$('.form-error').text(" * Please Choose University");
		return false ; 
	}

	submitPData['name'] = $('.sName').val();
	submitPData['age'] = $('.sAge').val();
	submitPData['email'] = $('.sEmail').val();
	submitPData['gender'] = getGender($('input:radio[name="radio-inline-1"]:checked').val());
	submitPData['course'] = $('input:radio[name="radio-inline-2"]:checked').val()
	submitPData['cast'] = $('input:radio[name="radio-inline-3"]:checked').val();
	submitPData['pcity'] = $('.select-city-1').select2('val');

	submitEData['school'] = $('.sCollege').val();
	submitEData['uni'] = $('.select-uni').select2('val');
	submitEData['ecity'] = $('.select-city-2').select2('val');
	submitEData['percentage'] = $('.sMarks').val();
	submitEData['passout'] = $('.select-passyear').select2('val');;
	submitEData['gap'] = $('.select-gap').select2('val');
	
	console.log(submitPData) ;
	validation = validateForm()

	if(!validation){
		Purl = '/api/v1/studentsp/'
		requestType = "POST";
		contentType = 'application/json';
		dataType= 'json';
		
		doAjax(Purl, requestType, JSON.stringify(submitPData), contentType, dataType, fillPDetails);
	}else{
		return ;
	}
	
	

	

}

function fillPDetails(data){
	pDetails = data ;
	pStudentid = pDetails.resource_uri ;

	submitEData['student'] = pStudentid
	

	console.log(submitEData) ;

	Eurl = '/api/v1/studentse/'
	requestType = "POST";
	contentType = 'application/json';
	dataType= 'json';
		
	doAjax(Eurl, requestType, JSON.stringify(submitEData), contentType, dataType, fillEDetails);


}

function fillEDetails(data){
	EDetailsData = data

	notifyData = {}
	notifyData['name'] = data.student.name
	notifyData['email'] = data.student.email
	notifyData['appid'] = 'App'+ data.student.id
	notifyData['course'] =data.student.course

	Nurl = '/notification/'
	requestType = "POST";
	contentType = 'application/json';
	dataType= 'json';
		
		
	doAjax(Nurl, requestType, JSON.stringify(notifyData), contentType, dataType);

	$('.form-success').removeClass('hidden');
	$('.form-success .appid').text('App'+ data.student.id);
	$('.form-error').addClass('hidden');
	document.getElementById("Detailform").reset();

}

function vallidateEmail(email){

	if(email == '' || typeof email == "undefined" || email == null){
		
		return false ; 
	}else{
		var pattern = new RegExp(/^((([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+(\.([a-z]|\d|[!#\$%&'\*\+\-\/=\?\^_`{\|}~]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+)*)|((\x22)((((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(([\x01-\x08\x0b\x0c\x0e-\x1f\x7f]|\x21|[\x23-\x5b]|[\x5d-\x7e]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(\\([\x01-\x09\x0b\x0c\x0d-\x7f]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))))*(((\x20|\x09)*(\x0d\x0a))?(\x20|\x09)+)?(\x22)))@((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?$/i);
    	return pattern.test(email);
	}
}

function validateNumber(num){
	intnum = parseInt(num)
	if(isNaN(intnum)){
		return false ; 
	}else{
		return true ;
	}
}

function validatePercentage(percentage){
	floatnum = parseFloat(percentage).toFixed(2)
	if(isNaN(floatnum)){
		return false ; 
	}else{
		return true ;
	}
}

function validateForm(){

	if(submitPData['name'] == '' || typeof submitPData['name'] == "undefined" || submitPData['name'] == null){
		$('.form-success').addClass('hidden');
		$('.form-error').removeClass('hidden');
		$('.form-error').text(" * Please enter valid name");
		return false ;
	}

	if(!vallidateEmail(submitPData['email'])){
		$('.form-success').addClass('hidden');
		$('.form-error').removeClass('hidden');
		$('.form-error').text(" * Please enter valid email");
		return false ;
	}

	if(!validateNumber(submitPData['age'])){
		$('.form-success').addClass('hidden');
		$('.form-error').removeClass('hidden');
		$('.form-error').text(" * Please enter valid age");
		return false ;
	}

	if(submitEData['school'] == '' || typeof submitEData['school'] == "undefined" || submitEData['school'] == null){
		$('.form-success').addClass('hidden');
		$('.form-error').removeClass('hidden');
		$('.form-error').text(" * Please enter valid  school/college name");
		return false ;
	}

	if(!validatePercentage(submitEData['percentage'])){
		$('.form-success').addClass('hidden');
		$('.form-error').removeClass('hidden');
		$('.form-error').text(" * Please enter valid percentage");
	}

}

 function searchApp(){

 	searchid =  $('.seachId').val();
 	findid = searchid.slice(3);

 	appstr = searchid.slice(0, 3);

 	if(appstr != "App"){
 		$('.form-search-error').removeClass('hidden');
 		return ;
 	}

 	if(findid == "" || typeof findid == "undefined"){
 		$('.form-search-error').removeClass('hidden');
 		return ;
 	}

 	Furl = '/api/v1/studentse/?order_by=-percentage'
	requestType = "GET";
	contentType = 'application/json';
	dataType= 'json';
		
	doAjax(Furl, requestType, '', contentType, dataType, fillFDetails);

 }

function fillFDetails(data){
	recivedData = data;
	console.log(recivedData);

	recivedData_length =  recivedData.objects.length ; 

	for(i=0;i<recivedData_length;i++){
		if(findid == recivedData.objects[i].student.id){
			
			j = parseInt(recivedData.objects[i].student.id);
			k = i + 1 ;
			
			$('.appDetails').removeClass('hidden');
			$('.form-search-error').addClass('hidden');
			$('.stuName').text(recivedData.objects[i].student.name);
			$('.stuCourse').text(recivedData.objects[i].student.course);
			$('.stuEmail').text(recivedData.objects[i].student.email);
			$('.stuCollege').text(recivedData.objects[i].school);
			
			$('.stuid').text("App"+j);
			$('.stuUni').text(recivedData.objects[i].uni.name);
			$('.stuMerit').text(k);
			$('.stuCast').text(recivedData.objects[i].student.cast);
			console.log(recivedData.objects[i].student.gender);

			$('.stuGender').text(getGenderstr(recivedData.objects[i].student.gender));
			$('.stuPass').text(recivedData.objects[i].passout);
			break ;
		}else{
			
			$('.appDetails').addClass('hidden');
			$('.form-search-error').removeClass('hidden');
			$('.form-search-error').text('* No Data Found');
		}
	}


}