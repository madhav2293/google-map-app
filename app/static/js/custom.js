// PAGE RELATED SCRIPTS
$(document).ready(function() {
	pageSetUp();
	setHeader();
	populateStates();
});

$(window).load(function() {
	$('select').select2().on("change", function(e){
		$('.overlay').removeClass('hidden');
		$('.map-widget').addClass('hidden');
		populateMap(e.val);
	})

});

function populateMap(state){
	stateUrl = '/api/getWhetherData?state='+state
	requestType = "GET";
	contentType = 'application/json';
	dataType= 'json';
	doAjax(stateUrl, requestType, '', contentType, dataType, drawMap);
}

function drawMap(data){
	
	$('.map-widget').removeClass('hidden');
	pageHandler(data)
	$(window).unbind('gMapsLoaded');
	$(window).bind('gMapsLoaded', pageFunction);
	window.loadGoogleMaps();
	$('.overlay').addClass('hidden');
}

function populateStates(){
		
		stateUrl = '/api/getStates'
		requestType = "GET";
		contentType = 'application/json';
		dataType= 'json';
		$(".select-state").select2();
		doAjax(stateUrl, requestType, '', contentType, dataType, appendStates);
		
}

function appendStates(data){
	statesData =  data;
	statesData_length = statesData.data.length;
	statesArray = [] ;
	for(i=0;i<statesData_length;i++){
		state = statesData.data[i]; 
		var idx = $.inArray(state, statesArray);
		if (idx == -1) {
			statesArray.push(state);
			$('.select-state').append('<option value="'+ state +'" data-uri="'+ state +'">'+ state +'</option>')
		}
	}

}


function doAjax(url, type, data, contentType, dataType, successCallback, errorCallback)
{
	$.ajax({
		url : url,
		type : type,
		data : data,
		contentType : contentType,
		dataType : dataType,
		success : function(data){
			if(successCallback){
				console.log('callback')
				successCallback(data);
			}
		},
		beforeSend: function(jqXHR, settings) {
            // Pull the token out of the DOM.
            //jqXHR.setRequestHeader('X-CSRFToken', csrftoken);
        },
		error : function(data){
			console.log(data)
			if(errorCallback){
				errorCallback(data);
			}
			
		} 
	});	
}

function setHeader(){
	$('.map-header').text('Us cities having temperature greater than 70F');
	$('.map-header').removeClass('hidden');
}

function pageHandler(data){
	
	pageFunction = function(){

		/*jslint smarttabs:true */
		colorful_style = [{

			"featureType" : "landscape",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "transit",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#d3d3d3"
			}, {
				"visibility" : "on"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.stroke",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "landscape",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#b1bc39"
			}]
		}, {
			"featureType" : "landscape.man_made",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#ebad02"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#416d9f"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#000000"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.text.stroke",
			"stylers" : [{
				"visibility" : "off"
			}, {
				"color" : "#ffffff"
			}]
		}, {
			"featureType" : "administrative",
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#000000"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#ffffff"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.icon",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#ebad02"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#8ca83c"
			}]
		}];

		// Grey Scale
		greyscale_style = [{
			"featureType" : "road.highway",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "landscape",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "transit",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.park",
			"stylers" : [{
				"visibility" : "on"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#d3d3d3"
			}, {
				"visibility" : "on"
			}]
		}, {
			"featureType" : "poi.medical",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.medical",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.stroke",
			"stylers" : [{
				"color" : "#cccccc"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#cecece"
			}]
		}, {
			"featureType" : "road.local",
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#808080"
			}]
		}, {
			"featureType" : "administrative",
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#808080"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#fdfdfd"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.icon",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#d2d2d2"
			}]
		}];

		// Black & White
		monochrome_style = [{
			"featureType" : "road.highway",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "landscape",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "transit",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#d3d3d3"
			}, {
				"visibility" : "on"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.stroke",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "landscape",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#ffffff"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#cecece"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#000000"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.text.stroke",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#ffffff"
			}]
		}, {
			"featureType" : "administrative",
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#000000"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#000000"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.icon",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "off"
			}]
		}];

		// Retro
		metro_style = [{
			"featureType" : "transit",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#d3d3d3"
			}, {
				"visibility" : "on"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.stroke",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "landscape",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#eee8ce"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#b8cec9"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#000000"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.text.stroke",
			"stylers" : [{
				"visibility" : "off"
			}, {
				"color" : "#ffffff"
			}]
		}, {
			"featureType" : "administrative",
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#000000"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#ffffff"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.stroke",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.icon",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#d3cdab"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#ced09d"
			}]
		}, {
			"featureType" : "poi",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}];

		// Night
		nightvision_style = [{
			"featureType" : "landscape",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "transit",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#d3d3d3"
			}, {
				"visibility" : "on"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.stroke",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "landscape",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"hue" : "#0008ff"
			}, {
				"lightness" : -75
			}, {
				"saturation" : 10
			}]
		}, {
			"elementType" : "geometry.stroke",
			"stylers" : [{
				"color" : "#1f1d45"
			}]
		}, {
			"featureType" : "landscape.natural",
			"stylers" : [{
				"color" : "#1f1d45"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#01001f"
			}]
		}, {
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#e7e8ec"
			}]
		}, {
			"elementType" : "labels.text.stroke",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#151348"
			}]
		}, {
			"featureType" : "administrative",
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#f7fdd9"
			}]
		}, {
			"featureType" : "administrative",
			"elementType" : "labels.text.stroke",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#01001f"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#316694"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.icon",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#1a153d"
			}]

		}];

		// Night Light
		nightvision_highlight_style = [{
			"elementType" : "geometry",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"hue" : "#232a57"
			}]
		}, {
			"featureType" : "road.highway",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "landscape",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"hue" : "#0033ff"
			}, {
				"saturation" : 13
			}, {
				"lightness" : -77
			}]
		}, {
			"featureType" : "landscape",
			"elementType" : "geometry.stroke",
			"stylers" : [{
				"color" : "#4657ab"
			}]
		}, {
			"featureType" : "transit",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.stroke",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#0d0a1f"
			}]
		}, {
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#d2cfe3"
			}]
		}, {
			"elementType" : "labels.text.stroke",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#0d0a1f"
			}]
		}, {
			"featureType" : "administrative",
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#ffffff"
			}]
		}, {
			"featureType" : "administrative",
			"elementType" : "labels.text.stroke",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#0d0a1f"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#ff9910"
			}]
		}, {
			"featureType" : "road.local",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#4657ab"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.icon",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#232a57"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#232a57"
			}]
		}, {
			"featureType" : "poi",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}];

		// Papiro
		old_paper_style = [{
			"elementType" : "geometry",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#f2e48c"
			}]
		}, {
			"featureType" : "road.highway",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "transit",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi.park",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"color" : "#d3d3d3"
			}, {
				"visibility" : "on"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.stroke",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "landscape",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#f2e48c"
			}]
		}, {
			"featureType" : "landscape",
			"elementType" : "geometry.stroke",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#592c00"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#a77637"
			}]
		}, {
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#592c00"
			}]
		}, {
			"elementType" : "labels.text.stroke",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#f2e48c"
			}]
		}, {
			"featureType" : "administrative",
			"elementType" : "labels.text.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#592c00"
			}]
		}, {
			"featureType" : "administrative",
			"elementType" : "labels.text.stroke",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#f2e48c"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#a5630f"
			}]
		}, {
			"featureType" : "road.highway",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "on"
			}, {
				"color" : "#592c00"
			}]
		}, {
			"featureType" : "road",
			"elementType" : "labels.icon",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "water",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi",
			"elementType" : "geometry.fill",
			"stylers" : [{
				"visibility" : "off"
			}]
		}, {
			"featureType" : "poi",
			"elementType" : "labels",
			"stylers" : [{
				"visibility" : "off"
			}]
		}];
				
		// One color - Change the hue value for your desired color
		mono_color_style = [{
			"stylers" : [{
				"hue" : "#ff00aa"
			}, {
				"saturation" : 1
			}, {
				"lightness" : 1
			}]
		}];
		
		
		
		$this = $("#map_canvas");
		$zoom_level = ($this.data("gmap-zoom") || 5);
		$data_lat = ($this.data("gmap-lat") || 38.8833);
		$data_lng = ($this.data("gmap-lng") || 77.0167);
		$xml_src = ($this.data("gmap-src") || "xml/gmap/pins.xml");

		greyStyleMap = new google.maps.StyledMapType(greyscale_style, {
			name: "Greyscale"
		}),
		monoChromeStyleMap = new google.maps.StyledMapType(monochrome_style, {
			name: "Mono Chrome"
		}),
		metroStyleMap = new google.maps.StyledMapType(metro_style, {
			name: "Metro"
		}),
		nightvisionStyleMap = new google.maps.StyledMapType(nightvision_style, {
			name: "Nightvision"
		}),
		nvisionhstyleMap = new google.maps.StyledMapType(nightvision_highlight_style, {
			name: "Nightvision Light"
		}),
		oPaperStyleMap = new google.maps.StyledMapType(old_paper_style, {
			name: "Old Paper"
		}),
		colorfulStyleMap = new google.maps.StyledMapType(colorful_style, {
			name: "Colorful"
		}),
		monoColorStyleMap = new google.maps.StyledMapType(mono_color_style, {
			name: "One Color"
		});
		
		loadMap(data);
	};

}

function loadMap(mapData){
	centerLatLng = new google.maps.LatLng(37.09024, -95.712891);
	mapOptions = {
		zoom: 3,
		center: centerLatLng,
		//disableDefaultUI: true,
		//mapTypeId : google.maps.MapTypeId.ROADMAP
		mapTypeControlOptions: {
			mapTypeIds: [google.maps.MapTypeId.TERRAIN, 'colorful_style', 'greyscale_style',
				'monochrome_style', 'metro_style', 'nightvision_style', 'nightvision_highlight_style',
				'old_paper_style', 'mono_color_style'
			]
		}
	},

	bounds = new google.maps.LatLngBounds(),
	infowindow = new google.maps.InfoWindow(),
	map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

	map.mapTypes.set('colorful_style', colorfulStyleMap);
	map.mapTypes.set('greyscale_style', greyStyleMap);
	map.mapTypes.set('monochrome_style', monoChromeStyleMap);
	map.mapTypes.set('metro_style', metroStyleMap);
	map.mapTypes.set('nightvision_style', nightvisionStyleMap);
	map.mapTypes.set('nightvision_highlight_style', nvisionhstyleMap);
	map.mapTypes.set('old_paper_style', oPaperStyleMap);
	map.mapTypes.set('mono_color_style', monoColorStyleMap);

	//map.setMapTypeId(google.maps.MapTypeId.TERRAIN);
	map.setMapTypeId('metro_style');

		markers = [];
		
		infoWindowContent = []
			
		for(i=0;i<mapData.data.length;i++){
			markers.push([mapData.data[i].city,mapData.data[i].lat,mapData.data[i].lon])
		}
		
		for(i=0;i<mapData.data.length;i++){
			infoWindowContent.push(['<div class="info_content">' +
				'<h3><b>'+ mapData.data[i].city +'</b></h3>' +
				'<p><b>Latitude :</b>'+ mapData.data[i].lat +'</p>' +
				'<p><b>Longitude :</b>'+ mapData.data[i].lon +'</p>' +
				'<p><b>Temperature :</b>'+ mapData.data[i].temp +'</p>' +
				'</div>'])
		}
	
	

		// Display multiple markers on a map
		var infoWindow = new google.maps.InfoWindow(), marker, i;

		// Loop through our array of markers & place each one on the map
		for( i = 0; i < markers.length; i++ ) {
			var position = new google.maps.LatLng(markers[i][1], markers[i][2]);
			bounds.extend(position);
			marker = new google.maps.Marker({
				position: position,
				map: map,
				title: markers[i][0]
			});

			// Allow each marker to have an info window
			google.maps.event.addListener(marker, 'click', (function(marker, i) {
				return function() {
					infoWindow.setContent(infoWindowContent[i][0]);
					infoWindow.open(map, marker);
				}
			})(marker, i));

		}
}