from flask import Flask,jsonify
from flask import render_template
from flask import request
from flask import Response
from api.whether import *
application = Flask(__name__, static_url_path='/static')
application.debug = True

@application.route("/api/getWhetherData")
def getWhetherData():
	try:
		w = Whether()
		return jsonify(w.getWhetherDetails(request.args.get('state')))
	except:
		import traceback
		print traceback.format_exc()
	
@application.route("/api/getStates", methods=['GET'])
def getStates():
	w = Whether()
	return jsonify(w.stateNames)

@application.route("/")
def home(name=None):
    return render_template('gmap.html', name=name)

if __name__ == "__main__":
    application.run(host='0.0.0.0')