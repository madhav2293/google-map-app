import pyowm
import csv
from pyowm.webapi25.location import *
class Whether():

	def __init__(self):
		self.stateCityMappingDic = {}
		self.stateNames = {}
		self.stateNames['data'] = []
		
		with open('./utils/data.csv') as csvfile:
			fileReader = csv.reader(csvfile, delimiter=',')
			for data in fileReader:
				if data[2] != 'Canonical Name':
					cityName = data[2].split(',')[0]
					if len(data[2].split(',')) == 4:
						stateName = data[2].split(',')[2]
					elif len(data[2].split(',')) == 3:
						stateName = data[2].split(',')[1]
					
					if not stateName in self.stateCityMappingDic:
						self.stateCityMappingDic[stateName] = [cityName]
					else:
						self.stateCityMappingDic[stateName].append(cityName)
		
		temp = []
		for key in self.stateCityMappingDic:
			if key is not None:
				temp.append(key)
		
		self.stateNames['data'] = sorted(temp)
		
		
	def getWhetherDetails(self,state):
		
		try:
			owm = pyowm.OWM('19ccbd10cc7eef944ab9231e71a84ff5')
			responseData = []
			if self.stateCityMappingDic[state]:
				try:
					for cityName in self.stateCityMappingDic[state]:
						observation = owm.weather_at_place(cityName+',us')
						w = observation.get_weather()
						temp = w.get_temperature('fahrenheit')['temp']
						if float(temp) > 70:
							data = {}
							lat = observation.get_location().get_lat()
							lon = observation.get_location().get_lon()
							data['lat'] = lat
							data['lon'] = lon
							data['temp'] = str(temp)+'F'
							data['city'] = cityName
							responseData.append(data)
						if len(responseData) > 10:
							break
					return {'success':True,'data':responseData}
				except Exception,e:
					return {'success':False,'errorMsg':'Error while fetching data'}
			else:
				return {'success':False,'errorMsg':'No Data found'}
		except Exception,e:
			return {'success':False,'errorMsg':'Error while fetching data'}

			

